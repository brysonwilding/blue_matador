package chucknorris

import (
	"fmt"

	"github.com/urfave/cli"
)

var Command = cli.Command{
	Name:  "chucknorris",
	Usage: "Gets a joke from api.chucknorris.io",
	Subcommands: []cli.Command{
		{
			Name:  "jokeCategory",
			Usage: "Gets a joke in the given category",
			Action: func(c *cli.Context) {
				chuck := new(ChuckNorris)
				chuck.WindupKick()
				joke, err := chuck.JokeCategory(c.Args().First())
				if err != nil {
					fmt.Printf("Error: %v\n", err)
					return
				}
				if len(joke) == 0 {
					// Non-nullable strings... so shoehorning the interface
					fmt.Printf("null")
					return
				}
				fmt.Println(joke)
			},
		},
		{
			Name:  "jokeSearch",
			Usage: "Searches for a joke matching an input string",
			Action: func(c *cli.Context) {
				chuck := new(ChuckNorris)
				chuck.WindupKick()
				joke, err := chuck.JokeSearch(c.Args().First())
				if err != nil {
					fmt.Printf("Error: %v\n", err)
					return
				}
				if joke == "" {
					// Non-nullable strings... so shoehorning the interface
					fmt.Printf("null")
					return
				}
				fmt.Println(joke)
			},
		},
	},
}
