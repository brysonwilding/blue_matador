package chucknorris

import (
	"errors"
	"math/rand"
	"time"

	"gopkg.in/h2non/gentleman.v2"
)

type ChuckNorris struct {
	// This is useless, but fun!
	beard            bool
	gentleman_client *gentleman.Client
}

type ChuckNorrisJokeResponse struct {
	IconUrl string `json:"icon_url"`
	Id      string `json:"id"`
	Url     string `json:"url"`
	Value   string `json:"value"`
}

type ChuckNorrisSearchResponse struct {
	Total   int                       `json:"total"`
	Results []ChuckNorrisJokeResponse `json:"result"`
}

// CommonName: Init()
func (chuck *ChuckNorris) WindupKick() {
	client := gentleman.New()
	client.URL("https://api.chucknorris.io/")
	chuck.gentleman_client = client
	chuck.beard = true
}

// CommonName: IsValid()
func (chuck *ChuckNorris) ReadyToAttack() bool {
	if chuck.gentleman_client != nil {
		return true
	}
	return false
}

func (chuck *ChuckNorris) createRequest(path string) (request *gentleman.Request) {
	request = chuck.gentleman_client.Request()
	request.Path(path)
	return
}

/*
  Gets a joke from the Chuck API
*/
func (chuck *ChuckNorris) JokeCategory(category string) (string, error) {

	if !chuck.ReadyToAttack() {
		return "", errors.New("WindupKick must be executed to initialize the gentleman!")
	}

	request := chuck.createRequest("/jokes/categories")
	response, err := request.Do()
	if err != nil {
		return "", err
	}

	var categories []string
	err = response.JSON(&categories)
	if err != nil {
		return "", err
	}

	// Iterate since it's returned as an array...
	var categoryExists bool
	for _, v := range categories {
		if v == category {
			categoryExists = true
			break
		}
	}

	if !categoryExists {
		return "", nil
	}

	jokeReq := chuck.createRequest("/jokes/random")
	jokeReq.SetQuery("category", category)

	jokeResponse, err := jokeReq.Do()
	if err != nil {
		return "", err
	}

	var joke ChuckNorrisJokeResponse
	err = jokeResponse.JSON(&joke)
	if err != nil {
		return "", err
	}

	return joke.Value, nil
}

/*
  Queries a joke from the Chuck API
*/
func (chuck *ChuckNorris) JokeSearch(query string) (string, error) {

	if !chuck.ReadyToAttack() {
		return "", errors.New("ChuckNorris must be initiallized!")
	}

	req := chuck.createRequest("/jokes/search")
	req.SetQuery("query", query)

	response, err := req.Send()
	if err != nil {
		return "", err
	}

	joke := new(ChuckNorrisSearchResponse)
	err = response.JSON(&joke)
	if err != nil {
		return "", err
	}

	jokeLength := len(joke.Results)
	if len(joke.Results) == 0 {
		// Empty result set
		return "", nil
	}

	// Rand will be really large...
	rand.Seed(time.Now().UnixNano())
	return joke.Results[rand.Int()%jokeLength].Value, nil
}
