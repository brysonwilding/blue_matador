* Blue Matador Coding Assignment

This project implements urfave/cli so there are documentation on parameters can be found using the `-h` flag on any subcommand

There's much to be improved, but I tried as best I could to adhere to the time limit

There are tests, most of which are implemented for isPalindrome and the stats. Use `make test` to run them

Sample Commands:
```
./blue_matador isPalindrome 1221
true

./blue_matador isPalindrome 108743123
false

./blue_matador isPalindrome racecar
false //Only integer inputs

./blue_matador chucknorris jokeCategory money
When Chuck Norris sends in his taxes, he sends blank forms and includes only a picture of himself, crouched and ready to attack. Chuck Norris has not had to pay taxes, ever.

./blue_matador chucknorris jokeCategory notcool
null //nil internally

./blue_matador chucknorris jokeSearch blue
Teacher: why is the sky blue? Student: the sky is blue because Chuck Norris's favorite color is blue. Teacher: 100/100

./blue_matador chucknorris jokeSearch "he's not that cool"
null //obviously

./blue_matador calculateStats "1 2 3 4 5"
Mean: 3.000000
Min: 1
Max: 5
Median: 3.000000
Sum: 15
Standard Deviation: 1.414214

./blue_matador calculateStats "100 150 1099 64 250 1492"
Mean: 525.000000
Min: 64
Max: 1492
Median: 250.000000
Sum: 3155
Standard Deviation: 558.862088
```

:wq