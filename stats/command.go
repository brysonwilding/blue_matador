package stats

import (
	"fmt"
	"github.com/urfave/cli"
)

var Command = cli.Command{
	Name:  "calculateStats",
	Usage: "Standard math operations",
	Action: func(c *cli.Context) {
		stats := CalculateStats(c.Args().Get(0))
		fmt.Printf("Mean: %f\n", stats.GetMean())
		fmt.Printf("Min: %d\n", stats.GetMin())
		fmt.Printf("Max: %d\n", stats.GetMax())
		fmt.Printf("Median: %f\n", stats.GetMedian())
		fmt.Printf("Sum: %d\n", stats.GetSum())
		fmt.Printf("Standard Deviation: %f\n", stats.GetStdev())
	},
}
