package stats

import (
	"math"
	"sort"
	"strconv"
	"strings"
)

type statistic struct {
	original string
	list     []int
	isSorted bool
	length   int
	mean     float64
	min      int
	max      int
	median   float64
	sum      int
	stdev    float64
}

// Constructor
func newStatistic(numList string) *statistic {
	newStatistic := new(statistic)
	newStatistic.original = numList
	newStatistic.computeList()
	newStatistic.sortList()
	newStatistic.length = len(newStatistic.list)
	return newStatistic
}

// Accessors
// That which you compose, expose
func (i *statistic) GetOriginal() string {
	return i.original
}

func (i *statistic) GetList() []int {
	return i.list
}

func (i *statistic) GetMean() float64 {
	return i.mean
}

func (i *statistic) GetMin() int {
	return i.min
}

func (i *statistic) GetMax() int {
	return i.max
}

func (i *statistic) GetMedian() float64 {
	return i.median
}

func (i *statistic) GetSum() int {
	return i.sum
}

func (i *statistic) GetStdev() float64 {
	return i.stdev
}

// Computation
func (i *statistic) computeList() {
	if i.list != nil {
		return
	}

	splitNumList := strings.Split(i.original, " ")

	for _, j := range splitNumList {
		value, _ := strconv.Atoi(j)
		i.list = append(i.list, value)
	}
}

func (i *statistic) sortList() {
	if i.isSorted {
		return
	}

	sort.Ints(i.list)
	i.isSorted = true
}

func (i *statistic) computeMean() {
	if i.mean != 0 {
		return
	}

	i.computeSum()
	i.mean = float64(i.sum / i.length)
}

func (i *statistic) computeMin() {
	if i.min != 0 {
		return
	}

	i.min = i.list[0]
}

func (i *statistic) computeMax() {
	if i.max != 0 {
		return
	}

	i.max = i.list[i.length-1]
}

func (i *statistic) computeMedian() {
	if i.median != 0 {
		return
	}

	if i.length%2 == 0 {
		i.median = float64(i.list[i.length/2])
		return
	}

	mid1 := i.list[(i.length+1)/2]
	mid2 := i.list[(i.length-1)/2]
	i.median = float64((mid1 + mid2) / 2)
}

func (i *statistic) computeSum() {
	if i.sum != 0 {
		return
	}

	for _, j := range i.list {
		i.sum += j
	}
}

func (i *statistic) computeStdev() {
	if i.stdev != 0 {
		return
	}

	var squaredDeviation float64
	i.computeMean()
	for _, j := range i.list {
		meanDelta := float64(j) - i.mean
		squaredDeviation += meanDelta * meanDelta
	}

	/*
	   Could approximate the sqrt, but this is my one
	   call to the math lib
	*/
	i.stdev = math.Sqrt(squaredDeviation / float64(i.length))
}

func CalculateStats(numList string) (statsObject *statistic) {
	statsObject = newStatistic(numList)

	statsObject.computeMean()
	statsObject.computeMin()
	statsObject.computeMax()
	statsObject.computeMedian()
	statsObject.computeSum()
	statsObject.computeStdev()

	return
}
