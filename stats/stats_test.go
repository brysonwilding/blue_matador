package stats

import (
	"fmt"
	"math"
	"testing"
)

func arrange() (sut *statistic) {
	// This should be dymamic
	list := "5 3 4 1 2"
	sut = newStatistic(list)
	return
}

func TestMean(t *testing.T) {
	sut := arrange()
	sut.computeMean()
	desiredState := float64(3)

	if sut.mean != desiredState {
		fmt.Println(sut.list)
		t.Errorf("sut.mean == %f, want %f", sut.mean, desiredState)
	}
}

func TestMin(t *testing.T) {
	sut := arrange()
	sut.computeMin()

	min := math.MaxFloat64
	for _, j := range sut.list {
		min = math.Min(min, float64(j))
	}

	if int(min) != sut.min {
		fmt.Println(sut.list)
		t.Errorf("sut.min() == %d, want %f", sut.min, min)
	}
}

func TestMax(t *testing.T) {
	sut := arrange()
	sut.computeMax()

	max := float64(0)
	for _, j := range sut.list {
		max = math.Max(max, float64(j))
	}

	if int(max) != sut.max {
		fmt.Println(sut.list)
		t.Errorf("sut.max() == %d, want %d", sut.max, int(max))
	}
}

func TestMedian(t *testing.T) {}

func TestSum(t *testing.T) {
	sut := arrange()
	sut.computeSum()

	sum := 0
	for _, j := range sut.list {
		sum += j
	}

	if sum != sut.sum {
		fmt.Println(sut.list)
		t.Errorf("sut.sum() == %d, want %d", sut.sum, sum)
	}
}

func TestStdev(t *testing.T) {
	sut := arrange()
	sut.computeStdev()

	var squaredDeviation float64
	for _, j := range sut.list {
		meanDelta := float64(j) - sut.mean
		squaredDeviation += meanDelta * meanDelta
	}

	stdev := math.Sqrt(squaredDeviation / float64(sut.length))
	if stdev != sut.stdev {
		fmt.Println(sut.list)
		t.Errorf("list.stdev() == %f, want %f", sut.stdev, stdev)
	}

}
