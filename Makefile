blue_matador:
	go build -o "${PWD}/blue_matador"

.Phony: test
	find . -type d -exec go test {} \;
