package palindrome

import (
	"errors"
	"strconv"
	"strings"
	"unicode/utf8"
)

func IsPalindrome(input string) (bool, error) {
	_, err := strconv.Atoi(input)
	if err != nil {
		return false, errors.New("Input was not an integer")
	}

	// Lazy case invariant
	lowerInput := strings.ToLower(input)
	return input == reverse(lowerInput), nil
}

// Treating it as a string since a palindrome is a lexical property
func reverse(input string) string {
	// Convert input to byte array
	byteString := []byte(input)
	returnString := []string{}
	// Get the last character and put it in the front of the return string
	for len(byteString) > 0 {
		decodedRune, characterSize := utf8.DecodeLastRune(byteString)
		returnString = append(returnString, string(decodedRune))
		byteString = byteString[:len(byteString)-characterSize]
	}
	return strings.Join(returnString, "")
}
