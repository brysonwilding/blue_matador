package palindrome

import (
	"testing"
)

func TestIsPalindrome(t *testing.T) {
	palindromes := map[string]bool{
		"1010101":   true,
		"55555":     true,
		"-1000":     false,
		"500":       false,
		"15050":     false,
		"555666555": true,
	}

	for input, isRight := range palindromes {
		palindrome, _ := IsPalindrome(input)
		if palindrome != isRight {
			t.Errorf("Incorrect result for %s", input)
		}
	}
}
