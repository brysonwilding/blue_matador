package palindrome

import (
	"fmt"

	"github.com/urfave/cli"
)

var Command = cli.Command{
	Name:  "isPalindrome",
	Usage: "Tests an integer string to see if it's a palindrome",
	Action: func(c *cli.Context) {
		input := c.Args().Get(0)
		if input == "" {
			fmt.Println("Please provide an integer to test")
		}
		output, _ := IsPalindrome(input)
		fmt.Printf("%t\n", output)
	},
}
