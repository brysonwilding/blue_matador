package main

import (
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/brysonwilding/blue_matador/chucknorris"
	"gitlab.com/brysonwilding/blue_matador/palindrome"
	"gitlab.com/brysonwilding/blue_matador/stats"
)

func main() {
	app := cli.NewApp()
	app.Name = "blue_matador"
	app.Usage = "Runs a palindrome, a chuck norris joke or a stat calculation"
	app.Commands = []cli.Command{
		palindrome.Command,
		chucknorris.Command,
		stats.Command,
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
