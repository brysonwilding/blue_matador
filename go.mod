module gitlab.com/brysonwilding/blue_matador

go 1.12

require (
	github.com/urfave/cli v1.20.0
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980 // indirect
	gopkg.in/h2non/gentleman.v2 v2.0.3
)
